package com.paytm.exception;

public class InvalidAmountException extends Exception {

	private static final long serialVersionUID = 1L;

	public void chooseCorrectAmount() {
		System.out.println("Amount should be between 10 and 500 only");
	}
}
