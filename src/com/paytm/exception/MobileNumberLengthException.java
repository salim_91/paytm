package com.paytm.exception;

public class MobileNumberLengthException extends Exception {

	private static final long serialVersionUID = 1L;
	private static final int length = 10;

	public void lengthMismatch() {
		System.out.println("Moblie number should only be of length " + length);
	}

}
