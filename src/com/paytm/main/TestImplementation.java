package com.paytm.main;

import java.util.Map;

import com.paytm.serviceproviders.PayTM;

public class TestImplementation {

	public static Map<Integer, String> showDenominations(PayTM serviceProvider) {
		return serviceProvider.denominations();
	}

	public static void recharge(PayTM serviceProvider, String mobileNumber, int amount) {
		serviceProvider.recharge(mobileNumber, amount);
	}
}
