package com.paytm.main;

import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

import com.paytm.exception.InvalidAmountException;
import com.paytm.exception.MobileNumberLengthException;
import com.paytm.serviceproviders.Airtel;
import com.paytm.serviceproviders.Idea;
import com.paytm.serviceproviders.Vodafone;
import com.paytm.users.Users;

public class TestPayTM {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		boolean payTM = false;

		while (!payTM) {
			showInitialOptions();
			int option = 0;

			try {
				option = scanner.nextInt();
			} catch (InputMismatchException ex) {
				System.out.print("Only Integer values allowed :");
			}

			scanner.nextLine();
			switch (option) {
			case 1:
				Users loggedInUser = login();

				if (loggedInUser != null) {
					boolean loggedin = false;

					while (!loggedin) {
						showLoggedInOptions();
						int facility = 0;

						try {
							facility = scanner.nextInt();
						} catch (InputMismatchException ex) {
							System.out.print("Only Integer values allowed :");
						}

						scanner.nextLine();

						switch (facility) {
						case 1:
							addAmountInWallet(loggedInUser);
							break;
						case 2:
							recharge(loggedInUser);
							break;
						case 3:
							checkWalletBalance(loggedInUser);
							break;
						case 4:
							loggedin = true;
							loggedInUser = null;
							System.out.println("You have succesfully logged out");
							break;
						default:
							System.out.println("Incorrect Option");
							break;
						}

					}
				}
				break;
			case 2:
				register();
				break;
			case 3:
				forgotPassword();
				break;
			case 4:
				payTM = true;
				break;
			default:
				System.out.println("Incorrect Choice");
				break;
			}

		}
	}

	private static void showInitialOptions() {
		System.out.println("Press 1 - Login \n" + 
						   "Press 2 - Register \n" + 
						   "Press 3 - Forgot Password \n" + 
						   "Press 4 - to exit");
	}

	private static Users login() {
		System.out.println("Enter username");
		String username = scanner.nextLine();
		System.out.println("Enter Password");
		String password = scanner.nextLine();
		Users user = new Users(username, password);
		user = user.authenticateUser(username, password);
		if (user != null) {
			System.out.println("Welcome to PayTM");
			return user;
		}
		return null;
	}

	private static void register() {
		System.out.println("Enter username");
		String username = scanner.nextLine();
		System.out.println("Enter Password");
		String password = scanner.nextLine();
		Users user = new Users(username, password);
		if (user.addNewUser(username, password)) {
			System.out.println("You have successfully registered with PayTM");
		} else {
			System.out.println("username already exists, please try a unique username");
		}
	}

	private static void forgotPassword() {
		System.out.println("Enter username");
		String username = scanner.nextLine();
		Users user = new Users(username);
		if (user.changePassword(username)) {
			System.out.println("Enter new Password");
			String newPassword = scanner.nextLine();
			user.changePassword(username, newPassword);
		} else {
			System.out.println("User does not exists");
		}
	}

	private static void showLoggedInOptions() {
		System.out.println("Press 1 - Add Amount to wallet \n" + 
						   "Press 2 - Recharge \n" + 
						   "Press 3 - To check balance in wallet \n" + 
						   "Press 4 - to logout");
	}

	private static void addAmountInWallet(Users user) {
		System.out.println("Enter amount to be added in wallet");
		double amount = 0.0;
		try {
			amount = scanner.nextDouble();
		} catch (InputMismatchException e) {
			System.out.print("Only Numerical values allowed: ");
		}
		scanner.nextLine();
		if (user.addBalanceToWallet(amount)) {
			System.out.println(amount + " successfully added to your wallet");
		} else {
			System.out.println("Sorry something went wrong, please try again later");
		}
	}

	private static void checkWalletBalance(Users user) {
		double balance = user.balanceEnquiry();
		System.out.println("Your wallet balance is " + balance);
	}

	private static void recharge(Users user) {

		boolean serviceOperators = false;
		while (!serviceOperators) {
			System.out.println("Enter Mobile Number");
			String mobileNumber;
			try {
				mobileNumber = scanner.nextLine();
				if (mobileNumber.length() < 10 || mobileNumber.length() > 10) {
					throw new MobileNumberLengthException();
				}
			} catch (MobileNumberLengthException e) {
				e.lengthMismatch();
				return;
			}

			showAllAvailableOperators();
			System.out.println("Select Operator");
			int serviceProvider = scanner.nextInt();
			scanner.nextLine();
			Map<Integer, String> denom;

			switch (serviceProvider) {
			case 1:
				denom = TestImplementation.showDenominations(new Airtel());
				for (Map.Entry<Integer, String> entry : denom.entrySet()) {
					System.out.println(entry.getKey() + ": " + entry.getValue());
				}
				boolean status = false;

				while (!status) {
					System.out.println("Choose Amount from above available plans only");
					int amount = 0;
					try {
						amount = scanner.nextInt();
						if (amount < 10 || amount > 500) {
							throw new InvalidAmountException();
						}
					} catch (InputMismatchException e) {
						scanner.nextLine();
					} catch (InvalidAmountException exp) {
						exp.chooseCorrectAmount();
						scanner.nextLine();
					}

					switch (amount) {
					case 160:
						if (user.balanceEnquiry() < 160) {
							System.out.println("You do not have enough balance to proceed");
							status = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Airtel(), mobileNumber, 160);
							user.reduceBalanceOnSuccessfulRecharge(160.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status = true;
							serviceOperators = true;
						}
						break;
					case 50:
						if (user.balanceEnquiry() < 50) {
							System.out.println("You do not have enough balance to proceed");
							status = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Airtel(), mobileNumber, 50);
							user.reduceBalanceOnSuccessfulRecharge(50.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status = true;
							serviceOperators = true;
						}
						break;
					case 100:
						if (user.balanceEnquiry() < 100) {
							System.out.println("You do not have enough balance to proceed");
							status = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Airtel(), mobileNumber, 100);
							user.reduceBalanceOnSuccessfulRecharge(100.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status = true;
							serviceOperators = true;
						}
						break;
					case 120:
						if (user.balanceEnquiry() < 120) {
							System.out.println("You do not have enough balance to proceed");
							status = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Airtel(), mobileNumber, 120);
							user.reduceBalanceOnSuccessfulRecharge(120.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status = true;
							serviceOperators = true;
						}
						break;
					default:
						break;
					}

				}
				break;
			case 2:
				denom = TestImplementation.showDenominations(new Vodafone());
				for (Map.Entry<Integer, String> entry : denom.entrySet()) {
					System.out.println(entry.getKey() + ": " + entry.getValue());
				}
				boolean status1 = false;

				while (!status1) {
					System.out.println("Choose Amount from above available plans only");
					int amount = 0;
					try {
						amount = scanner.nextInt();
						if (amount < 10 || amount > 500) {
							throw new InvalidAmountException();
						}
					} catch (InputMismatchException e) {
						scanner.nextLine();
					} catch (InvalidAmountException exp) {
						exp.chooseCorrectAmount();
						scanner.nextLine();
					}

					switch (amount) {
					case 160:
						if (user.balanceEnquiry() < 160) {
							System.out.println("You do not have enough balance to proceed");
							status1 = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Vodafone(), mobileNumber, 160);
							user.reduceBalanceOnSuccessfulRecharge(160.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status1 = true;
							serviceOperators = true;
						}
						break;
					case 50:
						if (user.balanceEnquiry() < 50) {
							System.out.println("You do not have enough balance to proceed");
							status1 = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Vodafone(), mobileNumber, 50);
							user.reduceBalanceOnSuccessfulRecharge(50.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status1 = true;
							serviceOperators = true;
						}
						break;
					case 100:
						if (user.balanceEnquiry() < 100) {
							System.out.println("You do not have enough balance to proceed");
							status1 = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Vodafone(), mobileNumber, 100);
							user.reduceBalanceOnSuccessfulRecharge(100.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status1 = true;
							serviceOperators = true;
						}
						break;
					case 120:
						if (user.balanceEnquiry() < 120) {
							System.out.println("You do not have enough balance to proceed");
							status1 = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Vodafone(), mobileNumber, 120);
							user.reduceBalanceOnSuccessfulRecharge(120.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status1 = true;
							serviceOperators = true;
						}
						break;
					default:
						break;
					}

				}
				break;
			case 3:
				denom = TestImplementation.showDenominations(new Idea());
				for (Map.Entry<Integer, String> entry : denom.entrySet()) {
					System.out.println(entry.getKey() + ": " + entry.getValue());
				}
				boolean status2 = false;

				while (!status2) {
					System.out.println("Choose Amount from above available plans only");
					int amount = 0;
					try {
						amount = scanner.nextInt();
						if (amount < 10 || amount > 500) {
							throw new InvalidAmountException();
						}
					} catch (InputMismatchException e) {
						scanner.nextLine();
					} catch (InvalidAmountException exp) {
						exp.chooseCorrectAmount();
						scanner.nextLine();
					}

					switch (amount) {
					case 160:
						if (user.balanceEnquiry() < 160) {
							System.out.println("You do not have enough balance to proceed");
							status2 = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Idea(), mobileNumber, 160);
							user.reduceBalanceOnSuccessfulRecharge(160.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status2 = true;
							serviceOperators = true;
						}
						break;
					case 50:
						if (user.balanceEnquiry() < 50) {
							System.out.println("You do not have enough balance to proceed");
							status2 = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Idea(), mobileNumber, 50);
							user.reduceBalanceOnSuccessfulRecharge(50.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status2 = true;
							serviceOperators = true;
						}
						break;
					case 100:
						if (user.balanceEnquiry() < 100) {
							System.out.println("You do not have enough balance to proceed");
							status2 = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Idea(), mobileNumber, 100);
							user.reduceBalanceOnSuccessfulRecharge(100.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status2 = true;
							serviceOperators = true;
						}
						break;
					case 120:
						if (user.balanceEnquiry() < 120) {
							System.out.println("You do not have enough balance to proceed");
							status2 = true;
							serviceOperators = true;
						} else {
							TestImplementation.recharge(new Idea(), mobileNumber, 120);
							user.reduceBalanceOnSuccessfulRecharge(120.0);
							System.out.println("Your remaining balance is: " + user.balanceEnquiry());
							status2 = true;
							serviceOperators = true;
						}
						break;
					default:
						break;
					}

				}
				break;
			case 4:
				serviceOperators = true;
				break;
			default:
				System.out.println("Service Provider not found");
				break;
			}
		}
	}

	private static void showAllAvailableOperators() {
		System.out.println("Press 1 - Airtel \n" + 
						   "Press 2 - Vodafone \n" + 
						   "Press 3 - Idea \n" + 
						   "Press 4 - exit");
	}
}
