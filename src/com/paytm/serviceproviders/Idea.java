package com.paytm.serviceproviders;

import java.util.HashMap;
import java.util.Map;

public class Idea implements PayTM {

	private static Map<Integer, String> denominations = new HashMap<>();

	@Override
	public void recharge(String mobileNumber, int amount) {
		System.out
				.println("Your mobile number " + mobileNumber + " is successfully recharged by an amount of " + amount);
		System.out.println(denominations.get(amount));
	}

	@Override
	public Map<Integer, String> denominations() {
		denominations.put(50, "Talktime of 43");
		denominations.put(100, "Talktime of 92");
		denominations.put(120, "Extra Talktime of 150");
		denominations.put(160, "1.5 GB 3G high speed internet");
		return denominations;
	}
}
