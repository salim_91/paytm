package com.paytm.serviceproviders;

import java.util.Map;

public interface PayTM {

	public abstract void recharge(String mobileNumber, int amount);

	public abstract Map<Integer, String> denominations();

}
