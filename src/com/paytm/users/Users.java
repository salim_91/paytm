package com.paytm.users;

import java.util.ArrayList;
import java.util.List;

public class Users {

	private String username;
	private String password;
	private double balance;
	private static List<Users> userList = new ArrayList<>();

	public Users(String username) {
		this.username = username;
	}

	public Users(String username, String password) {
		this.username = username;
		this.password = password;
		this.balance = 0;
	}

	public String getUsername() {
		return username;
	}

	public boolean addBalance(double amount) {
		if (amount >= 0) {
			this.balance = this.balance + amount;
			return true;
		}
		return false;
	}

	public double balanceEnquiry() {
		return this.balance;
	}

	public Users authenticateUser(String username, String password) {
		Users user = checkIfUserExists(username);
		if (user != null) {
			if (user.password.equals(password)) {
				return user;
			} else {
				System.out.println("Password is incorrect");
				return null;
			}
		}
		System.out.println("User does not exists");
		return null;
	}

	// Declared this method as private to achieve Encapsulation
	private Users checkIfUserExists(String username) {
		for (Users user : userList) {
			if (user.username.equals(username)) {
				return user;
			}
		}
		return null;
	}

	public boolean addNewUser(String username, String password) {
		if (checkIfUserExists(username) != null) {
			return false;
		} else {
			userList.add(new Users(username, password));
			return true;
		}
	}

	// Method Overloading, overloaded changePassword method to achieve compile
	// time polymorphism
	public boolean changePassword(String username) {
		if (checkIfUserExists(username) != null) {
			return true;
		}
		return false;
	}

	// Method Overloading, overloaded changePassword method to achieve compile
	// time polymorphism
	public boolean changePassword(String username, String newPassword) {
		for (Users user : userList) {
			if (user.username.equals(username)) {
				user.password = newPassword;
				System.out.println("Password changed successfully");
				return true;
			}
		}
		System.out.println("Sorry! Something went wrong");
		return false;
	}

	public boolean addBalanceToWallet(double amount) {
		if (amount > 0) {
			this.balance = this.balance + amount;
			return true;
		}
		return false;
	}

	public void reduceBalanceOnSuccessfulRecharge(double amount) {
		this.balance -= amount;
	}
}
